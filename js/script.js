function showRegestration() {
  const bthRegistration = document.querySelectorAll(
    ".header__nav__list__section__item"
  );

  bthRegistration.forEach((e) => {
    e.classList.toggle("header__nav__list__section__item--hidden");
  });
}

function showForm() {
  document
    .querySelector(".registration")
    .classList.remove("registration--hidden");
}
function getRegistration(email, password) {
  fetch("https://ajax.test-danit.com/api/v2/cards/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer b72e46eb-301f-4f31-a118-033e84b35ab6`,
    },
    body: JSON.stringify({
      email: email,
      password: password,
    }),
  })
    .then((response) => response.text())
    .then((token) => {
      if (token === "b72e46eb-301f-4f31-a118-033e84b35ab6") {
        localStorage.setItem(
          "token",
          JSON.stringify({
            email: email,
            password: password,
          })
        );
        document
          .querySelector(".registration")
          .classList.add("registration--hidden");
        showRegestration();
      } else {
        document
          .querySelector(".form__not__found")
          .classList.remove("form__not__found--hidden");
      }
    });
}
showRegestration();
function dataVerification() {
  document.querySelector(".form__btn").addEventListener("click", (e) => {
    e.preventDefault();
    let email = document.querySelector(".form__email");
    let password = document.querySelector(".form__password");
    getRegistration(email.value, password.value);
  });
}
dataVerification();

// -------------------------------exid-----------------------

function creatCards() {
  (document.querySelector(".cards__container").innerHTML = renderCards()),
    chaingeDoctor(),
    sendCards();
}
function deleteCads() {
  document.querySelectorAll(".cards__exid").forEach((e) => {
    e.addEventListener("click", (e) => {
      e.path[1].remove();
    });
  });
}
deleteCads();
function renderCards() {
  return `  <form  class="card_form">
  <div class="cards__exid" onclick="deleteCads()">X</div>
  <div class="cards">
    <select name="doctor" id="doctor">
    <option value="Dentist">Стоматолог</option>
      <option value="Cardiologist">Кардіолог</option>
      <option value="Therapist">Терапевт</option>
    </select>
    <input type="text" name="visit" placeholder="Мета візиту" />
    <input type="text" name="description" placeholder="Короткий опис візиту" />
    <select name="prioritet" id="">
      <option value="ordinary">Звичайна</option>
      <option value="priority">Пріоритетна</option>
      <option value="Urgent">Невідкладна</option>
    </select>
    <input type="text" name="name" placeholder="П.І.Б." />
    <div class="cards__more">
    <div class="doctors_delete">
   <input type="text" name="lastVisit"  placeholder="Дата останнього відвідуванняк" />
  </div></div>
  </div>
  <button type="submit" class="submiot_cards">Створити запис</button>
</form>`;
}

// -------------------------------renderCards--------------------------
function chaingeDoctor() {
  let doctor = document.querySelector("#doctor"),
    rednderDiv = document.querySelector(".cards__more");

  doctor.addEventListener("change", (item) => {
    console.log(item.target.value);
    if (item.target.value === "Cardiologist") {
      deletMore();
      rednderDiv.insertAdjacentHTML("afterEnd", renderCardiologist());
    } else if (item.target.value === "Dentist") {
      deletMore();
      rednderDiv.insertAdjacentHTML("afterEnd", renderDentist());
    } else if (item.target.value === "Therapist") {
      deletMore();
      rednderDiv.insertAdjacentHTML("afterEnd", renderTherapist());
    } else {
    }
  });
}

function deletMore() {
  document.querySelector(".doctors_delete").remove();
}
function renderCardiologist() {
  return ` 
  <div class="doctors_delete">
   <input type="text" name="pressure" placeholder="звичайний тиск" />
   <input type="text" name="bodyWeight" placeholder="Індекс маси тіла" />
   <input type="text" name="transferredDiseases" placeholder="Перенесені захворювання серцево-судинної системи" />
   <input type="text" name="ageCardiol" placeholder="Вік" />
  </div>`;
}
function renderDentist() {
  return `
  <div class="doctors_delete">
   <input type="text" name="data" placeholder="Дата останнього відвідуванняк" />
  </div>
  `;
}
function renderTherapist() {
  return `
  <div class="doctors_delete">
   <input type="text" name="age" placeholder="Вік" />
  </div>
  `;
}
// ------------------------------------------------------cardsmore-------------------
// class Cards{
//   constructor(age,data,ageCardiol,transferredDiseases,bodyWeight,pressure,lastVisit,name)
// }

const sentData = async (url, data) => {
  console.log(data);
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer b72e46eb-301f-4f31-a118-033e84b35ab6`,
    },
    body: JSON.stringify(data),
  });
  return await response.json();
};

const sendCards = () => {
  const form = document.querySelector(".card_form");
  form.addEventListener("submit", (e) => {
    console.log(e);
    e.preventDefault();
    const formData = new FormData(form);
    const value = Object.fromEntries(formData.entries());
    console.log(value);

    sentData("https://ajax.test-danit.com/api/v2/cards", value);
    getCards();
  });
};
// ------------------------------------------fetch------------------
const getCards = () => {
  fetch("https://ajax.test-danit.com/api/v2/cards/1", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer b72e46eb-301f-4f31-a118-033e84b35ab6`,
    },
  })
    .then((response) => {
      response.json();
    })
    .then((response) => {
      console.log(response);
    });
};
